module org.itcolleg.test {
    requires javafx.controls;
    requires javafx.fxml;
    requires transitive javafx.graphics;

    opens org.itcolleg.test to javafx.fxml;
    exports org.itcolleg.test;
}
